package com.example.demo;


import org.springframework.data.jpa.repository.JpaRepository;

public interface CalcRepository extends JpaRepository<Calc, Long> {
}
